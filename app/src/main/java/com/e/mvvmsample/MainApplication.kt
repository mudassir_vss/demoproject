package com.e.mvvmsample

import android.app.Application
import com.e.mvvmsample.data.network.MyApi
import com.e.mvvmsample.data.network.NetworkConnectionInterceptor
import com.e.mvvmsample.data.repositories.UserRepository
import com.e.mvvmsample.db.AppDatabase
import com.e.mvvmsample.ui.auth.AuthViewModelFactory
import dagger.hilt.android.HiltAndroidApp
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MainApplication:Application(),KodeinAware {
    override val kodein: Kodein=Kodein.lazy {

        import(androidXModule(this@MainApplication))

        bind()from singleton { NetworkConnectionInterceptor(instance()) }
        bind()from singleton { MyApi(instance()) }
        bind()from singleton { AppDatabase(instance()) }
        bind()from singleton { UserRepository(instance(),instance()) }
        bind() from provider { AuthViewModelFactory(instance()) }
    }
}