package com.e.mvvmsample.data.repositories

import com.e.mvvmsample.data.network.MyApi
import com.e.mvvmsample.data.network.SafeApiRequest
import com.e.mvvmsample.data.network.responses.AuthResponse
import com.e.mvvmsample.db.AppDatabase
import com.e.mvvmsample.db.Entities.User

class UserRepository(
    private val api:MyApi,
private val db:AppDatabase):SafeApiRequest() {

    suspend fun userLogin(email:String,password:String):AuthResponse
    {
        return apiRequest {api.userLogin(email,password) }
    }

    suspend fun userSignUp(name:String,email:String,password:String):AuthResponse
    {
        return apiRequest {api.userSignUp(name,email,password) }
    }

    suspend fun saveUser(user: User)= db.getUserDao().upsert(user)

     fun getUser()=db.getUserDao().getUser()
}