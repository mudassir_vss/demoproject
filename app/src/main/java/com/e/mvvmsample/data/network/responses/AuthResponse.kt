package com.e.mvvmsample.data.network.responses

import com.e.mvvmsample.db.Entities.User

data class AuthResponse(
    val isSuccessful: Boolean?,
    val message: String?,
    val user: User?
)