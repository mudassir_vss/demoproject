package com.e.mvvmsample.db.Dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.e.mvvmsample.db.Entities.CURRENT_USER_ID
import com.e.mvvmsample.db.Entities.User

@Dao
interface UserDao {

    @Insert(onConflict =OnConflictStrategy.REPLACE)
    suspend fun upsert(user: User):Long


    @Query("Select * from user where uid=$CURRENT_USER_ID")
    fun getUser():LiveData<User>

}