package com.e.mvvmsample.ui.auth

import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModel
import com.e.mvvmsample.data.repositories.UserRepository
import com.e.mvvmsample.util.ApiException
import com.e.mvvmsample.util.Coroutines
import com.e.mvvmsample.util.NoInternetException

class AuthViewModel(private val userRepository: UserRepository) : ViewModel() {
    var email: String? = null
    var password: String? = null
    var passwordConfirm: String? = null
    var name: String? = null
    var authListener: AuthListener? = null

    fun getLoggedInUser() = userRepository.getUser()
    fun onSignUp(v: View) {
        Intent(v.context, SignUpActivity::class.java).also {
            v.context.startActivity(it)
        }
    }

    fun onLogin(v: View) {
        Intent(v.context, LoginActivity::class.java).also {
            v.context.startActivity(it)
        }
    }

    fun onLoginButtonClicked(v: View) {
        authListener!!.onStarted()
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            //error
            authListener!!.onFailed("Invalid email or password")
            return
        }
        //success
        Coroutines.main {
            try {
                val authResponse = userRepository.userLogin(email!!, password!!)
                authResponse.user.let {
                    authListener!!.onSuccess(it!!)
                    userRepository.saveUser(it)
                    return@main
                }

            } catch (e: ApiException) {
                authListener!!.onFailed(e.message!!)
            } catch (e: NoInternetException) {
                authListener!!.onFailed(e.message!!)
            }
        }
    }

    fun onSignUpButtonClicked(v: View) {
        authListener!!.onStarted()
        if (name.isNullOrEmpty()) {
            //error
            authListener!!.onFailed("Invalid name")
            return
        }
        if (email.isNullOrEmpty()) {
            //error
            authListener!!.onFailed("Invalid email")
            return
        }
        if (password.isNullOrEmpty()) {
            //error
            authListener!!.onFailed("Invalid password")
            return
        }
        if (password != passwordConfirm) {
            //error
            authListener!!.onFailed("Password not matched")
            return
        }

        //success
        Coroutines.main {
            try {
                val authResponse = userRepository.userSignUp(name!!, email!!, password!!)
                authResponse.user.let {
                    authListener!!.onSuccess(it!!)
                    userRepository.saveUser(it)
                    return@main
                }

            } catch (e: ApiException) {
                authListener!!.onFailed(e.message!!)
            } catch (e: NoInternetException) {
                authListener!!.onFailed(e.message!!)
            }
        }

    }
}