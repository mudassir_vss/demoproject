package com.e.mvvmsample.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.e.mvvmsample.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


    }
}