package com.e.mvvmsample.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.e.mvvmsample.R
import com.e.mvvmsample.databinding.ActivitySignUpBinding
import com.e.mvvmsample.databinding.ActivitySignUpBindingImpl
import com.e.mvvmsample.db.Entities.User
import com.e.mvvmsample.ui.home.HomeActivity
import com.e.mvvmsample.util.hide
import com.e.mvvmsample.util.show
import com.e.mvvmsample.util.toast
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.Kodein
import org.kodein.di.android.kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class SignUpActivity : AppCompatActivity(),AuthListener,KodeinAware {
    override val kodein by kodein()
    var binding:ActivitySignUpBinding?=null
    var viewModel:AuthViewModel?=null
    private val viewModelFactory:AuthViewModelFactory  by instance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_sign_up)
        viewModel=ViewModelProvider(this,viewModelFactory).get(AuthViewModel::class.java)
        binding!!.viewModel=viewModel
        viewModel!!.authListener = this
        viewModel!!.getLoggedInUser().observe(this, Observer { user ->
            if (user != null) {
                Intent(this, HomeActivity::class.java).also {
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
            }
        })
    }

    override fun onStarted() {
        progress_bar.show()
        toast("Started")
    }

    override fun onSuccess(user: User) {
        progress_bar.hide()
        //AppDatabase.invoke(this).getUserDao().upsert(user)
//       toast(user.name!!)
    }

    override fun onFailed(e: String) {
        toast(e)
        progress_bar.hide()
    }
}