package com.e.mvvmsample.ui.auth

import androidx.lifecycle.LiveData
import com.e.mvvmsample.data.network.responses.AuthResponse
import com.e.mvvmsample.db.Entities.User
import retrofit2.Response

interface AuthListener {
    fun onStarted()
    fun onSuccess(user: User)
    fun onFailed(e:String)
}